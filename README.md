My Gitpod dot files

Currently I don't need much so this is super simplistic, but I use Gitpod with IntelliJ and use it for Scala and Go development.
Neither plugin is shipped by default and so I wanted a way to install them without having to restart every time I create a new workspace.

This install file will install Scala and Go plugins, tied to the latest plugin for the Idea compatable version named in the install file, currently: 2021.3 — 2021.3.2

Clearly this could be streamlined but it didn't seem worth it for 2 plugins!

Fork away :)
